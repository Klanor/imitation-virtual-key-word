#include <iostream>
#include <memory>

// TODO: Implement vtable support for all three classes without using of virtual keyword.
// Do not change main() and, if possible, do not change class LevelTwo and class SideLevel.
// Hint: you can add some code to global scope!

//class LevelTwo;

class LevelOne {
private:
    int y = 1;
public:
    /* virtual */ void simple() {
        VirtualSimple(this);
    }

    virtual ~LevelOne() = default;

    int cool(int x) {
        return VirtualCool(this, x);
    }

    friend void VirtualSimple(LevelOne* obj);
    friend int VirtualCool(LevelOne* obj, int x);
};


class LevelTwo : public LevelOne {
private:
    int y = 2;
public:
    void simple() {
        std::cout << "LevelTwo: very simple." << std::endl;
    }

    int cool(int x) {
        std::cout << "LevelTwo: very cool!" << std::endl;
        return x+y;
    }
};

class SideLevel : public LevelOne {
private:
    int y = 3;
public:
    void simple() {
        std::cout << "SideLevel: quite simple." << std::endl;
    }

    int cool(int x) {
        std::cout << "SideLevel: quite cool!" << std::endl;
        return x+y;
    }
};


void VirtualSimple(LevelOne *obj) {
    LevelTwo* lv = dynamic_cast<LevelTwo*>(obj);
    SideLevel* sl = dynamic_cast<SideLevel*>(obj);

    if(lv != nullptr){
        lv->simple();
    }else if(sl != nullptr){
        sl->simple();
    }else{
        std::cout << "LevelOne: simple." << std::endl;
    }
}

int VirtualCool(LevelOne* obj, int x) {
    LevelTwo* lv = dynamic_cast<LevelTwo*>(obj);
    SideLevel* sl = dynamic_cast<SideLevel*>(obj);

    if(lv != nullptr){
        return lv->cool(x);
    }else if(sl != nullptr){
        return sl->cool(x);
    }else{
        std::cout << "LevelOne: simple." << std::endl;
        return obj->y+x;
    }
}

int main() {

    // Expected: LevelOne function calls
    LevelOne o;
    o.simple();
    o.cool(5);

    std::cout << std::endl;

    // Expected: LevelTwo function calls
    LevelTwo t;
    t.simple();
    t.cool(5);

    SideLevel s;

    std::cout << std::endl;

    // Expected: LevelTwo function calls
    LevelOne &ot = t;
    ot.simple();
    ot.cool(5);

    std::cout << std::endl;

    // Expected: SideLevel function calls
    LevelOne &os = s;
    os.simple();
    os.cool(5);

    std::cout << std::endl;

    // Expected: LevelTwo function calls
    LevelOne *otp = &t;
    otp->simple();
    otp->cool(5);

    std::cout << std::endl;

    // Expected: LevelTwo function calls
    LevelOne *ptr = new LevelTwo();
    ptr->simple();
    ptr->cool(5);

    return 0;
}
